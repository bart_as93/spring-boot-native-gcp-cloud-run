#!/bin/bash
#TO RUN SCRIPT ADD IMAGE TAG AS PARAMENTER: 'core_native_built_push_and_run.sh 0.0.1-SNAPSHOT'
set -x
set -e
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd  )"
ROOT_DIR="$( cd ${SCRIPT_DIR} && cd .. && pwd )"
cd $ROOT_DIR || exit # we are in root dir
docker image build --file ./core/Dockerfiles/Dockerfile -t europe-central2-docker.pkg.dev/t8ts-419616/core/core:native."$1" .
docker push europe-central2-docker.pkg.dev/t8ts-419616/core/t8t-core:native."$1"
gcloud run deploy core-api --image europe-central2-docker.pkg.dev/t8ts-419616/core/core:native."$1" --platform managed --region europe-central2 --allow-unauthenticated --max-instances=1
#$SHELL