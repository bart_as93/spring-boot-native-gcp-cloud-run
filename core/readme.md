[//]: # (TODO przenieść requiraments do glownego readme?)
# Local
## Prerequirements: 
- Java version = GraalVm jdk
- Visual studio 2022 17.1+ (with Desktop development with C++)

## Build native image:
`mvn clean -Pnative native:compile`

## How to run built image in windows:
`./target/core.exe`

# Docker as native image
## Build image - using docker multistage to build image on linux (only then gc=g1 is supported). 
To use multistage build you need a lot of ram available from docker - please change on ocker desktop Settings>Resources>Advanced>Memory - increase from 2 GB to at least 6 GB
Build will last around:  1110.9s (18,5 min)
`
docker build -f Dockerfiles/Dockerfile -t core:native.0.0.1-SNAPSHOT .
`
## Run image
`
docker run --rm --name native -p 8080:8080 core:native.0.0.1-SNAPSHOT
`

## Test purpose: Docker from jar - test version
`

`
## Test purpose: Docker from native build in windows
docker build -f Dockerfiles/Dockerfile.windows -t t8t-core:windows.0.0.1-SNAPSHOT .



## Push image to container registry (Where projectId = t8ts-419616, region of my App=europe-central2, repository=core)
### Push native image
`docker image tag t8t-core:native.0.0.1-SNAPSHOT europe-central2-docker.pkg.dev/t8ts-419616/core/t8t-core:native.0.0.1-SNAPSHOT`
`docker push europe-central2-docker.pkg.dev/t8ts-419616/core/t8t-core:native.0.0.1-SNAPSHOT`
### Push test image
`docker image tag t8t-core:test.0.0.1-SNAPSHOT europe-central2-docker.pkg.dev/t8ts-419616/core/t8t-core:test.0.0.1-SNAPSHOT`
`docker push europe-central2-docker.pkg.dev/t8ts-419616/core/t8t-core:test.0.0.1-SNAPSHOT`

# Google Cloud run



# Local testing
curl -X PUT --header "Api-Key: phPiPnbZ1Z7YA8w25blvuT0LPMLCgYGeGLuWP7pGMqeMLqAYlpQfpzmL9Noe1Nqz" -v http://localhost:8080/t8t/expire
curl -X PUT --header "Api-Key: phPiPnbZ1Z7YA8w25blvuT0LPMLCgYGeGLuWP7pGMqeMLqAYlpQfpzmL9Noe1Nqz" -v https://core-api-j4hifd5kaq-lm.a.run.app/t8t/expire
