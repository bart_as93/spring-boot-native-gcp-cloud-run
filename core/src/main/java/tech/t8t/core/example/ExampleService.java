package tech.t8t.core.example;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExampleService {

    private final ExampleRepository repository;

    public ExampleDto add(ExampleDto dto) {
        ExampleDocument doc =
                Optional.ofNullable(dto)
                        .map(t -> ExampleDocument.builder()
                                .text(dto.getText())
                                .build())
                        .orElseThrow(IllegalStateException::new);

        ExampleDocument saved = repository.save(doc).block();
        return new ExampleDto(saved.getId(), saved.getText());

    }

    public ExampleDto get(String id) {
        if (!StringUtils.hasText(id)) {
            throw new IllegalStateException("id is empty");
        }

        return repository
                .findById(id)
                .map(doc -> new ExampleDto(doc.getId(), doc.getText()))
                .block();
    }

}
