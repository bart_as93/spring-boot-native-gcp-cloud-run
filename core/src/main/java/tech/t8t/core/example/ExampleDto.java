package tech.t8t.core.example;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExampleDto {
    private String id;
    private String text;
}
