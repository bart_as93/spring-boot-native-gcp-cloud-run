package tech.t8t.core.example;

import com.google.cloud.firestore.annotation.DocumentId;
import com.google.cloud.spring.data.firestore.Document;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collectionName = "example")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExampleDocument {
    @DocumentId
    private String id;
    private String text;

}
