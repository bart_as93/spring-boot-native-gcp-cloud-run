package tech.t8t.core.example;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ExampleController {

    private final ExampleService exampleService;

    @PostMapping("/example")
    @ResponseStatus(HttpStatus.CREATED)
    public ExampleDto create(@RequestBody ExampleDto dto) {
        return exampleService.add(dto);
    }


    @GetMapping("/example/{id}")
    public ExampleDto readByCreatorName(@PathVariable String id) {
        return exampleService.get(id);
    }
}
