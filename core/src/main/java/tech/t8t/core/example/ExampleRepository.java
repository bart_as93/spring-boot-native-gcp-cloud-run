package tech.t8t.core.example;

import com.google.cloud.spring.data.firestore.FirestoreReactiveRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExampleRepository extends FirestoreReactiveRepository<ExampleDocument> {
}
