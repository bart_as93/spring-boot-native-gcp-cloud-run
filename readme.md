# Spring boot 3.2.4 as native image using java 22 GraalVM AOT connected to firestore and deployed to Cloud Run
## Pre configurations
### Cloud run + Artifacts registry + Firestore
1. Download gcloud CLI
2. Create GCP account and project and remember your project_id. 
### Gcloud Configuration
#### Google cloud artifact registry
`gcloud services enable artifactregistry.googleapis.com`
`gcloud auth configure-docker europe-central2-docker.pkg.dev`
`
gcloud artifacts repositories create core \
--repository-format=docker \
--location=europe-central2 \
--description="core api repository"
`
#### Cloud run configuration
`gcloud services enable run.googleapis.com`
#### Cloud firestore
1. Create firestore project in google cloud console
2. Change "core-firestore" to your firestore id in core/src/main/resources/application.yml:7
#### Local firestore emulator
`gcloud emulators firestore start`

## Build and push docker images to artifact registry
### Native docker image
Change below: "europe-central2-docker.pkg.dev/t8ts-419616" to "<artifact_registr_region>-docker.pkg.dev/<google_cloud_project_id>/core/" and change version from "0.0.1-SNAPSHOT" to whatever version you want
Native image need to be build in OS where you will run image. Best way is multi-stage docker. To run `docker image build` you need at least 6 GB ram provided to your docker. If you run it on windows you need to change resources settings in docker for windows and increase default 2GB to 6GB. 
Native build will take around 15-20 min
`
docker image build --file ./Dockerfiles/Dockerfile -t europe-central2-docker.pkg.dev/t8ts-419616/core/core:native.0.0.1-SNAPSHOT .
docker push europe-central2-docker.pkg.dev/t8ts-419616/core/core:native.0.0.1-SNAPSHOT
`
### Test (JIT compilation) docker image - fast for testing
`
mvn clean package
docker image build --file ./Dockerfiles/Dockerfile.test -t europe-central2-docker.pkg.dev/t8ts-419616/core/core:test.0.0.1-SNAPSHOT .
docker push europe-central2-docker.pkg.dev/t8ts-419616/core/t8t-core:test.0.0.1-SNAPSHOT
`
## Deploy native image to cloud run
Change in below script "europe-central2-docker.pkg.dev/t8ts-419616" to "<artifact_registr_region>-docker.pkg.dev/<google_cloud_project_id>/core/" 
Change also version "native.0.0.1-SNAPSHOT" to whatever you want.
Change region "europe-central2" to your region
```shell
gcloud run deploy core-api \
--image europe-central2-docker.pkg.dev/t8ts-419616/core/t8t-core:native.0.0.1-SNAPSHOT \
--platform managed \
--region europe-central2 \
--allow-unauthenticated \
--max-instances=1
```
## Deploy test image
```shell
gcloud run deploy core-api \
--image europe-central2-docker.pkg.dev/t8ts-419616/core/t8t-core:test.0.0.1-SNAPSHOT \
--platform managed \
--region europe-central2 \
--allow-unauthenticated \
--max-instances=3
```

## Check container endpoint
gcloud beta run services describe core-api --platform managed --region europe-central2 --format="value(status.url)"
curl $URL


## Scripts
You have two scripts in "scripts" directory. To run this scripts firstly change:
1. "europe-central2-docker.pkg.dev/t8ts-419616" to "<artifact_registr_region>-docker.pkg.dev/<google_cloud_project_id>/core/"
2. region "europe-central2" to your region in gcloud run command
example command to run script:
`./scripts/core_native_built_push_and_run.sh 0.0.1-SNAPSHOT` 

## Gitlab CI
`
gcloud iam service-accounts create CI-AND-CD-SERVICE-ACCOUNT --description="Service account for CI and CD needs" --display-name="ci and cd service account"
gcloud projects add-iam-policy-binding t8ts-419616 --member="serviceAccount:CI-AND-CD-SERVICE-ACCOUNT@t8ts-419616.iam.gserviceaccount.com" --role="roles/artifactregistry.admin"
gcloud projects add-iam-policy-binding t8ts-419616 --member="serviceAccount:CI-AND-CD-SERVICE-ACCOUNT@t8ts-419616.iam.gserviceaccount.com" --role="roles/run.invoker"
gcloud projects add-iam-policy-binding t8ts-419616 --member="serviceAccount:CI-AND-CD-SERVICE-ACCOUNT@t8ts-419616.iam.gserviceaccount.com" --role="roles/run.viewer"
gcloud projects add-iam-policy-binding t8ts-419616 --member="serviceAccount:CI-AND-CD-SERVICE-ACCOUNT@t8ts-419616.iam.gserviceaccount.com" --role="roles/run.developer"
gcloud projects add-iam-policy-binding t8ts-419616 --member="serviceAccount:CI-AND-CD-SERVICE-ACCOUNT@t8ts-419616.iam.gserviceaccount.com" --role="roles/artifactregistry.serviceAgent"
gcloud projects add-iam-policy-binding t8ts-419616 --member="serviceAccount:CI-AND-CD-SERVICE-ACCOUNT@t8ts-419616.iam.gserviceaccount.com" --role="roles/artifactregistry.repositories.downloadArtifacts"
gcloud projects add-iam-policy-binding t8ts-419616 --member="serviceAccount:CI-AND-CD-SERVICE-ACCOUNT@t8ts-419616.iam.gserviceaccount.com" --role="roles/iam.serviceAccountUser""
gcloud iam service-accounts gcp.json create key --iam-account=CI-AND-CD-SERVICE-ACCOUNT@t8ts-419616.iam.gserviceaccount.com
`
Next in UI of gitlab repository:
1. Settings > CI/CD > Variables > EXPAND > ADD variable
2. Set KEY: "GOOGLE_JSON_KEY" and paste gcp.json content to "Value" input text
3. Change in .gitlab-ci.yml:
    a. Change in line number 7: "bart_as93" to your user name, "spring-boot-native-gcp-cloud-run" to your repository name
    b. Change in line number 35: "europe-central2-docker.pkg.dev" to your artifact registry prefix (it depends on gcloud region that you use)
    c. Change in line number 36: "europe-central2-docker.pkg.dev/t8ts-419616/core/" to your <artifact_registr_region>-docker.pkg.dev/<google_cloud_project_id>/core/"
    d. Change in line number 37: "europe-central2-docker.pkg.dev/t8ts-419616/core/" to your <artifact_registr_region>-docker.pkg.dev/<google_cloud_project_id>/core/"
    e. Change in line number 48: "europe-central2-docker.pkg.dev" to your artifact registry prefix (it depends on gcloud region that you use)
    f. Change in line number 49: "t8ts-419616" to your google_cloud_project_id
    g. Change in line number 50: "europe-central2-docker.pkg.dev/t8ts-419616/core/" to your <artifact_registr_region>-docker.pkg.dev/<google_cloud_project_id>/core/" and "europe-central2" to your region in google cloud.

   
### Build core
eg. Go to Gitlab pipeline on gitlab website for me: https://gitlab.com/bart_as93/spring-boot-native-gcp-cloud-run/-/pipelines/new
VERSION=0.0.9-SNAPSHOT
SERVICE=core

## Deploy core
VERSION=0.0.9-SNAPSHOT
SERVICE=core
TASK=deploy


